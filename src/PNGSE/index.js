import React from "react";


export default class PNGSE extends React.Component {
  static defaultProps = {
    rate: 30,
    loop: true,
    frames: {
      sourceImage: "",
      framesInfo: {}
    }
  };

  constructor(props) {
    super(props);
    this.oImage = new Image();
    this.oCanvas = React.createRef();
    this.state = { width: 0, height: 0 }
    // 解析关键帧
    const { frames: { framesInfo } } = this.props;
    this.currentFrame = 0;
    this.keyFrames = Object.keys(framesInfo).sort();
  };

  componentDidMount() {
    const { frames: { sourceImage } } = this.props;
    this.oImage.src = sourceImage;
    this.oImage.addEventListener("load", this.playSequence);
  };

  componentWillUnmount() {
    this.oImage.removeEventListener("load", this.playSequence);
    clearInterval(this.timmer);
  };

  playCurrent = (ctx) => {
    // 播放每一帧
    try {
      const { frames: { framesInfo } } = this.props;
      const currentFrameInfo = this.keyFrames[this.currentFrame];
      const { x: startCutX, y: startCutY, width: cutWidth, height: cutHeight } = framesInfo[currentFrameInfo];
      const { width: renderWidth, height: renderHeight } = framesInfo[currentFrameInfo];
      this.setState({ width: cutWidth, height: cutHeight });
      ctx.clearRect(0, 0, cutWidth, cutHeight);
      ctx.drawImage(this.oImage, startCutX, startCutY, cutWidth, cutHeight, 0, 0, renderWidth, renderHeight);
    } catch (error) {
      console.log(error);
    };
  };

  playSequence = () => {
    // 播放png序列
    try {
      clearInterval(this.timmer);
      const { rate, loop } = this.props;
      const ctx = this.oCanvas.current.getContext("2d");
      this.timmer = setInterval(() => {
        try {
          if (this.currentFrame <= this.keyFrames.length - 1) {
            this.playCurrent(ctx);
            this.currentFrame = this.currentFrame + 1;
          } else {
            if (loop) {
              this.currentFrame = 0;
              this.playSequence(ctx);
            } else {
              clearInterval(this.timmer);
            };
          };
        } catch (error) {
          console.log(error);
        };
      }, rate);
    } catch (error) {
      console.log(error);
    };
  };

  render() {
    const { width, height } = this.state;
    const { rate, loop, frames, ...otherProps } = this.props;
    return (
      <canvas {...otherProps} width={width} height={height} ref={this.oCanvas} />)
  };

};