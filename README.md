### pngse-react

* 基于canvas

* 与 [@yushicheng/pngse](https://www.npmjs.com/package/@yushicheng/pngse )命令行工具搭配使用的react组件

> PNGSE组件简介

| 属性   | 类型                             | 默认值 | 说明                            |
| ------ | -------------------------------- | ------ | ------------------------------- |
| frames | {sourceImage: "",framesInfo: {}} | {}     | 由pngse命令行生成的关键帧信息   |
| rate   | number                           | 30     | 帧速率，该值基于setInterval函数 |
| loop   | boolean                          | true   | 是否循环播放                    |

* 先引入.compose文件夹

```javascript
import animationFrmaes from "./.compose";
```

* 将导入animationFrmaes设置到组件的frames属性上即可

```react
import React from "react"
import ReactDOM from "react-dom";
import PNGSE from "@yushicheng/PNGSE";

const mountNode=document.getElementById("root");
ReactDOM.render(<PNGSE frames={animationFrmaes}/>,mountNode)

```