import React from "react";
import ReactDOM from "react-dom";

import PNGSE from "../src";
import frames from "./animation/.compose";

class ComponentName extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  };

  render() {
    return (
      <PNGSE frames={frames} />)
  };
};


(function render() {
  const mountNode = document.createElement("div");
  document.body.appendChild(mountNode);
  ReactDOM.render((<ComponentName />), mountNode);
})();
